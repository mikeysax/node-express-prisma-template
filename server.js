// Server Dependencies
const Express = require('express');
const cluster = require('express-cluster');
const compression = require('compression');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const methodOverride = require('method-override');

// Database Dependencies
const { PrismaClient } = require('@prisma/client');
let prisma = new PrismaClient();
const models = require('require-dir-all')('./api/models');
Object.keys(models).forEach((modelName) =>
  prisma = prisma.$extends(models[modelName](prisma))
);
exports.prisma = prisma;

global.__DEVELOPMENT__ = process.env.NODE_ENV === 'development';
// TODO: If this is needed in the future
// if (__DEVELOPMENT__) {
// 	require('./env.js');
// }

let app;
const initializeExpressServer = (worker = { id: 1 }) => {
  // Initialize Express App
  app = new Express();
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(cookieParser());
  app.use(methodOverride());
  app.use(compression());
  app.use(Express.static(__dirname + '/dist'));

  const corsSettings = {
    origin: __DEVELOPMENT__ ? 'localhost' : 'http://test.production-url.com',
    methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE'],
    credentials: true,
    preflightContinue: false
  };
  app.use(cors(corsSettings));

  app.use('/api', require('./router'));

  const PORT = process.env.PORT || 8080;
  app.listen(PORT, error => {
    if (error) return console.error('Server Express Error:', error);
    console.log(`-- Server ${worker.id} listening on: ${PORT} --`);
  });
};

if (__DEVELOPMENT__) {
  initializeExpressServer();
} else {
  cluster(worker => initializeExpressServer(worker));
}

module.exports = app;
