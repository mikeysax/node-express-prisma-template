const { prisma } = require('../../server');

exports.index = async (req, res) => {
  try {
    const result = await prisma.todo.findMany();

    return res.status(200).json({ todos: result });
  } catch (error) {
    console.error('Error:', error);
    return res.status(404).json({ error });
  }
};

exports.show = async (req, res) => {
  const { id } = req.params;

  try {
    const result = await prisma.todo.findFirst({
      where: { id: parseInt(id) }
    });

    return res.status(200).json({ todo: result });
  } catch (error) {
    console.error('Error:', error);
    return res.status(404).json({ error });
  }
};

exports.create = async (req, res) => {
  const { title, content, authorId } = req.body;

  try {
    const result = await prisma.todo.create({
      data: {
        title,
        content,
        authorId: parseInt(authorId)
      },
    });

    return res.status(201).json({ todo: result });
  } catch (error) {
    console.error('Error:', error);
    return res.status(403).json({ error });
  }
};

exports.update = async (req, res) => {
  const { id } = req.params;
  const { title, content, done } = req.body;

  try {
    const result = await prisma.todo.update({
      where: { id: parseInt(id) },
      data: { title, content, done }
    })

    return res.status(200).json({ todo: result });
  } catch (error) {
    console.error('Error:', error);
    return res.status(404).json({ error });
  }
};

exports.destroy = async (req, res) => {
  const { id } = req.params;

  try {
    const result = await prisma.todo.delete({
      where: { id: parseInt(id) }
    });

    return res.status(200).json({ todo: result });
  } catch (error) {
    console.error('Error:', error);
    return res.status(404).json({ error });
  }
};

exports.markComplete = async (req, res) => {
  const { id } = req.params;

  try {
    const result = await prisma.todo.markCompleteById(id)

    return res.status(200).json({ todo: result });
  } catch (error) {
    console.error('Error:', error);
    return res.status(404).json({ error });
  }
};

exports.markIncomplete = async (req, res) => {
  const { id } = req.params;

  try {
    const result = await prisma.todo.markIncompleteById(id)

    return res.status(200).json({ todo: result });
  } catch (error) {
    console.error('Error:', error);
    return res.status(404).json({ error });
  }
};
