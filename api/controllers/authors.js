const { prisma } = require('../../server');

exports.index = async (req, res) => {
  try {
    const result = await prisma.author.findMany();

    return res.status(200).json({ authors: result });
  } catch (error) {
    console.error('Error:', error);
    return res.status(404).json({ error });
  }
};

exports.show = async (req, res) => {
  const { id } = req.params;

  try {
    const result = await prisma.author.findFirst({
      where: { id: parseInt(id) },
    });

    return res.status(200).json({ author: result });
  } catch (error) {
    console.error('Error:', error);
    return res.status(404).json({ error });
  }
};

exports.create = async (req, res) => {
  const { name } = req.body;

  try {
    const result = await prisma.author.create({
      data: {
        name
      },
    });

    return res.status(201).json({ author: result });
  } catch (error) {
    console.error('Error:', error);
    return res.status(403).json({ error });
  }
};

exports.update = async (req, res) => {
  const { id } = req.params;
  const { name } = req.body;

  try {
    const result = await prisma.author.update({
      where: { id: parseInt(id) },
      data: { name },
    });

    return res.status(200).json({ author: result });
  } catch (error) {
    console.error('Error:', error);
    return res.status(404).json({ error });
  }
};

exports.destroy = async (req, res) => {
  const { id } = req.params;

  try {
    const result = await prisma.author.delete({
      where: { id: parseInt(id) },
    });

    return res.status(200).json({ todo: result });
  } catch (error) {
    console.error('Error:', error);
    return res.status(404).json({ error });
  }
};

exports.searchByName = async (req, res) => {
  const { name } = req.body;

  try {
    const result = await prisma.author.searchByName(name);

    return res.status(200).json({ todo: result });
  } catch (error) {
    console.error('Error:', error);
    return res.status(404).json({ error });
  }
}
