module.exports = (prisma) => {
  return {
    name: 'AuthorExtension',
    model: {
      author: {
        async searchByName(name) {
          return await prisma.$queryRawUnsafe(
            'SELECT * FROM "Author" WHERE name ILIKE $1',
            `%${name}%`
          )
        }
      },
    },
  };
};
