module.exports = (prisma) => {
  return {
    name: 'TodoExtension',
    model: {
      todo: {
        async markCompleteById(id) {
          return await prisma.todo.update({
            where: { id: parseInt(id) },
            data: {
              done: true,
            },
          });
        },
        async markIncompleteById(id) {
          return await prisma.todo.update({
            where: { id: parseInt(id) },
            data: {
              done: false,
            },
          });
        }
      },
    },
  };
};
