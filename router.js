// Router Dependencies
const express = require('express');
const router = express.Router();

// Middleware

// Controllers
const authors = require('./api/controllers/authors')
const todos = require('./api/controllers/todos');

// Base Path /api->
router
  // Middleware
  // Example: .use(checkCookie)

  // Example Index Routes
  // Example: .get('/path', midleware, controllerAction)
  .get('/authors', authors.index)
  .post('/authors', authors.create)
  .get('/authors/:id', authors.show)
  .patch('/authors/:id', authors.update)
  .delete('/authors/:id', authors.destroy)
  .post('/authors/search', authors.searchByName)

  .get('/todos', todos.index)
  .post('/todos', todos.create)
  .get('/todos/:id', todos.show)
  .patch('/todos/:id', todos.update)
  .delete('/todos/:id', todos.destroy)
  .patch('/todos/:id/complete', todos.markComplete)
  .patch('/todos/:id/incomplete', todos.markIncomplete);

// -----------------------------------------------

module.exports = router;
